
TESTE PAULO


# Code repository
<https://gitlab.collabora.com/sysadmin/pglistener/pglistener>

# Package repository
<https://salsa.debian.org/pglistener-team/pglistener>

# Contact

Collabora SysAdmin Team <sysadmin@collabora.com>

# Manual

You can read the manual in manpage/pglistener-update.txt

# Installation hints

## Hooks

* Allow the pglisten user to run any commands in `/var/lib/pglisten/hooks` as
  root using sudo. This is useful for changing ownership of files.

## NSS

* Ensure that the DB backend for NSS is installed.

* Enable the db backend for for the passwd/group/shadow entries in
   `nsswitch.conf`.

* Create symlinks from `/var/lib/misc/*.db` to the generated files.
