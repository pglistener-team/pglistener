#!/usr/bin/env python3

from setuptools import setup

setup(
    name='pglistener',
    version='5.0',
    author='Collabora SysAdmin Team',
    author_email='sysadmin@collabora.com',
    license='GPLv2+',
    license_file='COPYING',
    packages=[
        'pglistener',
    ],
    scripts=[
        'useradm/bin/pua',
        'bin/pglistener',
        'bin/pglistener-update',
    ],
    install_requires=[
        'python3-psycopg2',
    ],
    extra_requires={
        'systemd': ['sdnotify'],
    },
    classifiers=[
        'License :: OSI Approved :: GNU General Public License v2 or later (GPLv2+)',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Topic :: Utilities',
    ],
)
