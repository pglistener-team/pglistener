NAME
  pglistener - postgres listener.

DESCRIPTION
 pglistener is a daemon that generates files from Postgres database.

 It is a tool that listens for notifies from a Postgres database
 and then calls out to a number of hooks which do everything from updating
 the password database to updating accounts in RT.

USAGE

  Config and hooks live under:

    /var/lib/pglistener
    /var/lib/pglistener/hooks
    /etc/pglistener
    /etc/pglistener/conf.d

  It is fine to restart postgres while pglistener is running.

  If pglistener crashes for whatever reason, it can be restarted as a
  normal service.

  Pglistener can in general be restarted at any time, it's quick to
  start and there are no interruptions to services or authentication.

SETTING UP

  Install a usable pglistener config in /etc/pglistener/pglistener.conf
  and should be mode 600, user pglistener.

  Make symlinks for databases in /var/lib/misc:

    cd /var/lib/misc
    ln -s ../pglistener/group.db
    ln -s ../pglistener/passwd.db
    ln -s ../pglistener/shadow.db

  If you have ssh keys for everybody on the host, add the following two lines to /etc/ssh/sshd_config:

    AuthorizedKeysFile /root/%u/.ssh/authorized_keys
    AuthorizedKeysFile2 /var/lib/misc/sshkeys/%u

  Make sure pam_mkhomedir is set up, /etc/pam.d/common-session should contain:

    session required pam_mkhomedir.so

AUTHOR
  The pglistener was written by Rob Bradford <rob@robster.org.uk>,
  Robert McQueen <robert.mcqueen@bluelinux.co.uk>,
  Dafydd Harries <dafydd.harries@collabora.co.uk>,
  Tollef Fog Heen <tfheen@err.no>,
  Maxime Buquet <pep@bouah.net>,
  Jordi Mallach <jordi@debian.org>,
  Vivek Das Mohapatra <vivek.dasmohapatra@collabora.com>
  Paulo Henrique de Lima Santana <phls@debian.org>
  Bruna Pinos de Oliveira <bruna.pinos@collabora.com>
  Mark Kennedy <mark.kennedy@collabora.com>
  Stelios Milidonis <stelios.milidonis@collabora.com>
  Collabora SysAdmin Team <sysadmin@collabora.com>

  For a complete list of authors, please open AUTHORS file.

  This manual page was written by Paulo Henrique de Lima Santana (phls) <phls@debian.org> for the
  Debian project (but may be used by others).
